import requests
import streamlit as st
import pandas as pd

from time import sleep
from datetime import time
import plotly.express as px
from login import login as login_dj


BASE_URL = 'http://crawlers.fontedeprecos.com.br'


@st.cache
def login(username, password):
    logged, cookies = login_dj(username, password)
    return logged, cookies


@st.cache
def get_bases(cookies):
    try:
        data = requests.get(f'{BASE_URL}/metrics/bases/',
                            cookies=cookies).json()
    except Exception as e:
        st.error(e)
        return None

    print(data)
    bases_items = []
    bases_avisos = []
    bases_captcha = []

    for tipo_base in data:
        match tipo_base['tipo']:
            case 'i':
                bases_items.append(tipo_base['base'])
            case 'a':
                bases_avisos.append(tipo_base['base'])

    return bases_items, bases_avisos, bases_captcha


@st.cache
def get_metrics(base, periodo, tipo, cookies):
    try:
        data = requests.get(
            f"{BASE_URL}/metrics/crawlers/?base={base}&period={periodo}&tipo={tipo[0].lower()}", cookies=cookies).json()
    except Exception as e:
        print(e)
        data = []

    return data


layout = 'wide' if 'logged' in st.session_state else 'centered'
print(layout)

st.set_page_config(
    page_title='Dashboard',
    page_icon='🚀',
    layout=layout
)


if 'logged' in st.session_state:
    st.title('🚀 Dashboard')
    st.write('Aqui você encontra métricas do serviço de Crawlers.')

    tipo_selected = st.sidebar.selectbox(
        'Escolha uma tipo:', ['Itens', 'Avisos'])

    items_base, avisos_base, captcha_base = get_bases(
        st.session_state['cookies'])

    match tipo_selected:
        case 'Itens':
            options = items_base
        case 'Avisos':
            options = avisos_base

    base_selected = st.sidebar.selectbox(
        'Selecione uma base:', options=options)

    periodo_selected = st.sidebar.selectbox(
        'Selecione um periodo(Dias):', [7, 15, 30])

    data = get_metrics(base_selected, periodo_selected,
                       tipo_selected, st.session_state['cookies'])

    col_total, col_tempo, col_ult_raspagem = st.columns(3)

    with col_total:
        st.metric(label='Total Raspado', value=data['total'])

    with col_tempo:
        time_total = 0
        time_length = 0

        for time in data['data']:
            time_length += 1
            time_total += float(time['execution_time'])

        med_time = round((time_total / time_length), 2)

        if len(data['data']) >= 2:
            med_time_delta = round(float(
                data['data'][0]['execution_time']) - float(data['data'][-1]['execution_time']), 2)
        else:
            med_time_delta = 0

        st.metric(label='Med. Tempo', value=med_time, delta=med_time_delta)

    with col_ult_raspagem:
        if len(data['data']) >= 2:
            ult_rasp_delta = data['data'][0]['total_added'] - \
                data['data'][-1]['total_added']
        else:
            ult_rasp_delta = 0
        st.metric(label='Ult. Raspagem',
                  value=data['data'][0]['total_added'], delta=ult_rasp_delta)

    data_grapy = []

    for item in data['data']:
        data_grapy.append([item['date'].split(' ')[0], item['total_added']])

    dt = pd.DataFrame(data_grapy, columns=['Data', 'Total'])

    bar_chart = px.bar(dt,
                       x='Data',
                       y='Total',
                       text='Total',
                       color_discrete_sequence=['#F63366']*len(dt),
                       template='plotly_white')
    st.plotly_chart(bar_chart, use_container_width=True)

    with st.expander("Estatísticas para nerds"):
        st.dataframe(data['data'])
        st.json(data)
else:
    placeholder = st.empty()

    with placeholder.container():
        st.header('Login')
        username = st.text_input('Username')
        password = st.text_input('Password', type='password')
        bt_login = st.button('Login')

    if bt_login:
        logged, cookies = login(username, password)

        if logged:
            st.session_state['logged'] = True
            st.session_state['cookies'] = cookies

            placeholder.empty()
            st.button('ir para dashboard')
        else:
            st.warning('Verifique suas credencias, e tente novamente.')
