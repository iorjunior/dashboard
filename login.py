from requests import session

SESSION = session()
BASE_URL = "http://crawlers.fontedeprecos.com.br"


def get_csrf():
    response = SESSION.get(f"{BASE_URL}/api-auth/login/")
    csrf = str(response.content).split('name="csrfmiddlewaretoken"')[
        1].split('value="')[1].split('">')[0]

    return csrf


def login(username, password):
    csrf = get_csrf()

    payload = {
        'csrfmiddlewaretoken': csrf,
        'next': '/',
        'username': username,
        'password': password,
        'submit': 'Log in',
    }

    SESSION.post(f"{BASE_URL}/api-auth/login/", data=payload)

    cookies = SESSION.cookies.get_dict()

    if cookies.get("sessionid") is not None:
        return True, cookies
    else:
        return False, {}
